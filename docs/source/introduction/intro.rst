AiiDA VASP Plugin Introduction
==============================

The AiiDA VASP Plugin aims to provide a full support for running VASP_ calculations using the AiiDA_ package.

The User Interface is based heavily on the VASP support already provided by the `Materials Project`_ through pymatgen_.
Main reasons for this are:
	* to support a **standard VASP interface** across different high-throughput frameworks.
	* to avoid code duplication and utilize the mature and well supported pymatgen code base.

The *philosophy* behind this plugin can be summarized as follows: 
	*Take a well defined VASP input and return parsed data in a form the User has specified - no more, no less.*

In order to facilitate the latter, we have implemented **Parser Instructions**, a simple way to customize and extend the parsing capabilities of the output parser.

Current Developments
====================

The plugin is still being actively developed. The current version of the plugin is operational but still needs work, 
both under the hood and on the user interface.

*Under the hood -* we aim to isolate Pymatgen from the plugin code base, making it an optional dependency. The current version of the plugin makes extensive use of the Pymagen VASP I/O functions, both from the user interface side and internally. We wish to make the core of the plugin minimalistic and stable, i.e. thoroughly tested and easily maintainable.

As a separate issue, we need to decide how to store VASP POTCAR objects in the database. Storing the full object is desireable from a data provenance point of view but, it could lead to leagal issues when the AiiDA database is shared, since VASP users are *not* allowed to share the pseudopotentials.

:Note: there will likely be a change in how the POTCAR and INCAR are stored in the future. 
       Currently they are stored as a raw copy of the corresponding Pymatgen dictionary representation.

*User Interface -* will be improved. Our intention here is to fully support, on equal footing, the natural workflows of people who are used to AiiDA and those who are used to Pymagen. The Pymatgen objects will still make a legitimate input, as originally intended. We are in the process of adding more object conversion tools to facilitate going back-and-forth between the two representations shamelessly.

.. _Materials Project: http://www.materialsproject.org/
.. include:: ../references.txt
