.. _full_pmg_input:

Setting up Pymatgen Input - Full Code
=====================================

.. literalinclude:: ./SubmittingJob_pmg.py
	:lines: 1-8, 15-72
