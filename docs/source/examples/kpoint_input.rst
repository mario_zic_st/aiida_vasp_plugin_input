.. _kpoints_input:

Importing Pymatgen Kpoints
==========================

The plugin supports importing Pymatgen Kpoint object in 4 different modes: Monkhorst, Line, Reciprocal, and Cartesian.

Here we will cover in more details how to import each of these kpoint modes in a VaspCalculation.

Reciprocal, Monkhorst & Line Mode
---------------------------------

Pymatgen Kpoints generated using: *Reciprocal*, *Kpoints.monhorst_automatic* and *automatic_linemode* methods 
require only calling the import function:

.. code-block:: python

    calc.import_kpoints(kpoins_object)

and the Kpoints should be automatically converted to AiiDA KpointsData object internally.

Cartesian
---------

In order to import *Cartesian* coordinates we need to know the unit cell vectors. 
The coordinates are always stored in the reciprocal format internally.

We can pass the structure using the import function like this: 

.. code-block:: python

    calc.import_kpoint(kpoins_object, structure=poscar.strucuture)

**Note** that we can pass either an AiiDA StructureData object or a Pymatgen Structure, like in the example above.

Unit cell information is stored in the KpointsData object!

.. Important::
    Kpoints stored in cartesian coordiantes will be returned by the export method in the reciprocal format.
    We may change this in the future !

Storing the Unit Cell
---------------------

If we want to store the unit cell information inside the *KpointsData* object, i.e. to store it in the database, 
we can set the *keep_structure* flag like this:

.. code-block:: python

    calc.import_kpoints(kpoins_object, structure=poscar.strucuture, keep_strucuture=True)

Note that now we **must** specify the structure object as well !


